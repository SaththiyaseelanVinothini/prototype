package creationalPattern;

public class StudentDecord implements Student {

	 private int id;  
	   private String name, course;  
	   private String batch;  
	   private String address;  
	      
	   public StudentDecord(){  
	            System.out.println("   Student Records of Bcas Campus ");  
	            System.out.println("---------------------------------------------");  
	            System.out.println("sid"+"\t"+"sname"+"\t"+"course"+"\t"+"batch"+"\t\t"+"saddress");  
	      
	}  
	  
	 public  StudentDecord(int id, String name, String course, String batch, String address) {  
	          
	        this();  
	        this.id = id;  
	        this.name = name;  
	        this.course = course;  
	        this.batch = batch; 
	        this.address = address;  
	    }  
	
	public void showRecord(){  
	          
	        System.out.println(id+"\t"+name+"\t"+course+"\t"+batch+"\t"+address);  
	   }  
	  
	    @Override  
	    public Student getClone(){  
	          
	        return new StudentDecord(id,name,course,batch,address);  
	    } 

}
