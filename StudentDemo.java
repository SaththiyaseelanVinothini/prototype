package creationalPattern;
import java.io.BufferedReader;  
import java.io.IOException;  
import java.io.InputStreamReader;  

public class StudentDemo {

	public static void main(String[] args) throws IOException {  
        
        BufferedReader br =new BufferedReader(new InputStreamReader(System.in));  
        System.out.print("Enter Student Id: ");  
        int sid=Integer.parseInt(br.readLine());  
        System.out.print("\n");  
          
        System.out.print("Enter Student Name: ");  
        String sname=br.readLine();  
        System.out.print("\n");  
          
        System.out.print("Enter Student course: ");  
        String scourse=br.readLine();  
        System.out.print("\n");  
          
        System.out.print("Enter Student Address: ");  
        String saddress=br.readLine();  
        System.out.print("\n");  
          
        System.out.print("Enter Student batch: ");  
        String sbatch= br.readLine();  
        System.out.print("\n");  
           
        StudentDecord e1=new StudentDecord(sid,sname,scourse,sbatch,saddress);  
          
        e1.showRecord();  
        System.out.println("\n");  
        //StudentDecord e2=(StudentDecord) e1.getClone();  
       // e2.showRecord();  
    }     
}
