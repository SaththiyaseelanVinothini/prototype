package creationalPattern;

public interface Student {

	public Student getClone();
}
